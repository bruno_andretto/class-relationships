package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Person {
    @Id
    @GeneratedValue
    public Long id;

    public String firstname;

    public String surname;

    @ManyToOne(cascade=CascadeType.ALL)
    public Department department;
}
