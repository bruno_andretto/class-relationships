package models;

import javax.persistence.*;
import java.util.List;


@Entity
@NamedQuery(name = "Department.findByTelephone",
        query = "select u from Department u where u.telephone = ?1")
public class Department {
    @Id
    @GeneratedValue
    public Long id;

    public String name;

    public String telephone;

    @OneToMany(cascade= CascadeType.ALL, targetEntity = Person.class, mappedBy ="department")
    public List<Person> people;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Office.class)
    public List<Office> office;


}
