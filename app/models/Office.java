package models;

import javax.persistence.*;
import java.util.List;


@Entity
public class Office {
    @Id
    @GeneratedValue
    public Long id;

    public String building;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Department.class, mappedBy ="office")
    public List<Department> depo;

}