package models;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

@Named
@Singleton
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    Department findByTelephone(String telephone);

}