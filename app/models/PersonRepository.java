package models;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public interface PersonRepository extends CrudRepository<Person, Long> {

    @Query("select u from Person u where u.department.name = ?1")
    Person findByDepartmentName(String departmentName);
}