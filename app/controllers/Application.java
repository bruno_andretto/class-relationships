package controllers;

import models.*;
import org.springframework.transaction.annotation.Transactional;
import play.mvc.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;


@Named
@Singleton
@Transactional
public class Application extends Controller {

    private final PersonRepository personRepository;

    private final DepartmentRepository departmentRepository;

    private final OfficeRepository officeRepository;


    // We are using constructor injection to receive a repository to support our desire for immutability.
    @Inject
    public Application(final PersonRepository personRepository, final DepartmentRepository departmentRepository, final OfficeRepository officeRepository ) {

        this.personRepository = personRepository;
        this.departmentRepository = departmentRepository;
        this.officeRepository = officeRepository;

    }

    public Result index() {

        final Department department = new Department();
        department.name= "Software";
        department.telephone= "1111-1111";
        final Department savedDepartment = departmentRepository.save(department);

        final Department department2 = new Department();
        department2.name= "Hardware";
        department2.telephone= "2222-2222";
        final Department savedDepartment2 = departmentRepository.save(department2);

        Iterable<Department> departments = departmentRepository.findAll();

        Iterable<Office> offices = officeRepository.findAll();

        return ok(views.html.index.render(departments, offices));

    }


    public Result create(long id, long id2) {


        /* Cria uma pessoa e coloca no departamento do id recebido */
        final Person person = new Person();
        person.firstname = "Bruce";
        person.surname = "Smith";
        person.department = departmentRepository.findOne(id);
        final Person savedPerson = personRepository.save(person);

        final Person person2 = new Person();
        person2.firstname = "Jonas";
        person2.surname = "Silva";
        person2.department = departmentRepository.findOne(id2);
        final Person savedPerson2 = personRepository.save(person2);


        /* Acha o departamento pra atrelar depois */
        final Department depo = departmentRepository.findOne(id);
        final Department depo2 = departmentRepository.findOne(id2);

        /* Cria dois offices e coloca no departamento do id recebido */
        final Office office = new Office();
        office.building = "Office 1";
        List<Department> departments = new ArrayList<>();
        departments.add(depo);
        departments.add(depo2);
        office.depo = departments;
        final Office savedOffice= officeRepository.save(office);

        final Office office2 = new Office();
        office2.building = "Office 2";
        office2.depo = departments;
        final Office savedOffice2= officeRepository.save(office2);


        final Person retrievedPerson = personRepository.findOne(savedPerson.id);

        final Office retrievedOffice1 = officeRepository.findOne(savedOffice.id);
        final Office retrievedOffice2 = officeRepository.findOne(savedOffice2.id);
        List<Office> offices = new ArrayList<>();
        offices.add(retrievedOffice1);
        offices.add(retrievedOffice2);

        /* falta adicionar os offices nos departamentos */

        depo.office.addAll(offices);
        depo2.office.addAll(offices);

        return ok(views.html.create.render(retrievedPerson, offices));

    }

    public Result showDepartment(long id) {

        final Department department = departmentRepository.findOne(id);

        return ok(views.html.showDepartment.render(department));
    }


    public Result showOffice(long id) {

        final Office office = officeRepository.findOne(id);

        return ok(views.html.showOffice.render(office));
    }

    public Result searchDepartment() {

        String tel = "2222-2222";
        String department = "Software";
        Department foundDepartment = departmentRepository.findByTelephone(tel);
        Person foundPerson = personRepository.findByDepartmentName(department);

        return ok(views.html.searchDepartment.render(foundPerson));
    }

}

